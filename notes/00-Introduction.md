# Introduction

ZenHaskell is a compendium of templates and strategies for quickstarting and
delivering application protoypes. We leverage stack, nix and gitlab to provide
an a quick route to an application demo.

We believe that type-driven development is the fastest, clearest and most
robust way to develop any application. Sketching out the types works to
simultaneously both capture use case requirements and express the resulting
program behaviour. Iteratively refining the types provides 

ZenHaskell offers ready-made infrastructure both custom up-to-date dockers
carrying prebuilt CI tools from nixpkgs. All of the template repositories come
pre-equipped with gitlab ci, so all you need to do is fork and start coding.

Most recent development of all templates, accelerators and manuals can be found
on [gitlab](https://gitlab.com/zenhaskell?sort=name_asc).

# Docker Accelerators

ZenHaskell projects use a custom
[docker](https://cloud.docker.com/repository/docker/zenhaskell/foundation)
accelerator covering the bases for Haskell CI with nix.

Includes: bash, cabal-install, cachix, dhall, git, hlint, nix, stack and
stack2nix.

# Pandoc-Based Web Templates

* [shakebook](http://zenhaskell.gitlab.io/shakebook)

Shakebook is a comprehensive technical documentation generator using pandoc,
shake and nix.

* [hakyll-template](http://zenhaskell.gitlab.io/hakyll-template)

The default hakyll template, optimised for Gitlab.

* [onesheet-template](http://zenhaskell.gitlab.io/onesheet-template/poster.pdf)

A onesheet latex/markdown pdf template using Pandoc and Shake.

# Application Templates

There are two main styles of template. One vein of template uses standard haskell with no default
language extensions and relies heavily on [docopt](https://hackage.haskell.org/package/docopt) and
[turtle](https://hackage.haskell.org/package/turtle). The other vein leverages [RIO](https://hackage.haskell.org/package/rio)
and [etc](https://hackage.haskell.org/package/rio) and has all RIO-suggested language extensions on by default.

## Docopt/Turtle Templates

* [docopt-template](https://gitlab.com/zenhaskell/docopt-template)

A very basic docopt template to make simple command line applications.

* [docopt-turtle-template](https://gitlab.com/zenhaskell/docopt-turtle-template)

A command line application template that extends docopt-template with [turtle](https://hackage.haskell.org/package/turtle)

* [docopt-turtle-gtk-template](http://gitlab.com/locallycompact/docopt-turtle-gtk-template)

A GUI template for making configurable GTK applications using docopt and turtle.

* [docopt-turtle-cloudhaskell-template](http://gitlab.com/locallycompact/docopt-turtle-cloudhaskell-template)

A template for building distributed applications using cloudhaskell.

## RIO/etc templates

* [rio-etc-template](https://gitlab.com/zenhaskell/rio-etc-template)

A basic CLI application template using the RIO prelude and etc for configuration management.

* [rio-etc-gtk-template](https://gitlab.com/zenhaskell/rio-etc-gtk-template)

A GUI application using RIO, etc and gi-gtk.

* [rio-etc-scotty-template](https://gitlab.com/zenhaskell/rio-etc-scotty-template)

An application for prototyping rest servers using the RIO, etc and [scotty](https://hackage.haskell.org/package/scotty)
